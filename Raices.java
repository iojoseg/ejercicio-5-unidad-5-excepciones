import java.util.Scanner;

public class Raices {

public static void main(String args[]){

  double a,b,c;
  RaicesSegundoGrado rc;
  
  Scanner entrada = new Scanner(System.in);

  System.out.println("Ingresa el valor de a: ");
  a = entrada.nextDouble();
  System.out.println("Ingresa el valor de b: ");
  b = entrada.nextDouble();
  System.out.println("Ingresa el valor de c: ");
  c = entrada.nextDouble();
  

  try{
      rc = new RaicesSegundoGrado(a,b,c);
      rc.raices();
      rc.escribir();
 
  }catch(NoRaizRealException er){
   System.out.println(er.getMessage());
  }
  catch(CoefAceroException er){
  System.out.println(er.getMessage());
  }

}

}
