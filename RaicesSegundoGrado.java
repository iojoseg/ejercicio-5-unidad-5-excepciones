
public class RaicesSegundoGrado{

 private double a,b,c;
 private double r1,r2;
 
 public RaicesSegundoGrado(double a, double b, double c){
 
 this.a = a;
 this.b = b;
 this.c = c;
 }
 public void raices() throws NoRaizRealException, CoefAceroException
 {
 double resul;
  if(Math.pow(b,2.0) < 4*a*c)
    throw new NoRaizRealException("Discriminante negativo", a,b,c);
  if(a == 0)
    throw new CoefAceroException("No ecuaciones De segundo grado  ");
  resul = Math.sqrt(b*b - 4*a*c);
  r1 = (-b - resul) / (2*a);
  r2 = (-b + resul) / (2*a);

 } 
 
  public void escribir(){
  
  System.out.println("Raices de la ecuacion: r1 = "+ (double) r1);
  System.out.println("Raices de la ecuacion: r2 = "+ (double) r2);
  
  }

}