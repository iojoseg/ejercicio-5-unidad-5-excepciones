public class NoRaizRealException extends Exception{

private double a,b,c;

public NoRaizRealException(String nom, double a, double b, double c){

super(nom);
this.a = a;
this.b = b;
this.c = c;
}

public String getMessage(){

   return "Para los coeficientes " +(double)a +(double)b +(double)c + super.getMessage();
}

}